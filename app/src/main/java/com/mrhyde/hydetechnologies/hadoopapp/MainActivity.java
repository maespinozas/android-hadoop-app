package com.mrhyde.hydetechnologies.hadoopapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;


public class MainActivity extends Activity {
    private HashMap<String,Integer> hashDegree=new HashMap<String,Integer>();
    private HashMap<String,Integer> hashMetric=new HashMap<String,Integer>();
    public String sshresultSring="";
    public String d="";             //metric
    public String m="";             //degree

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //NOT RECOMENDABLE AT ALL, WILL CHANGE LATER FOR ASYNC
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Spinners
        final Spinner spinnerDegree=(Spinner)findViewById(R.id.spinnerDegree);
        final Spinner spinnerMetric=(Spinner)findViewById(R.id.spinnerMetric);
        final EditText editUser=(EditText)findViewById(R.id.editUser);
        final EditText editPass=(EditText)findViewById(R.id.editPass);
        final EditText editPort=(EditText)findViewById(R.id.editPort);
        final EditText editIP=(EditText)findViewById(R.id.editHostIp);
        //Submit button
        Button btnSubmit=(Button)findViewById(R.id.button);
        Button btnMSG=(Button)findViewById(R.id.buttonMsg);

        hashDegree.put("ECE", 1);
        hashDegree.put("CS", 2);
        hashDegree.put("EE",3);
        hashMetric.put("%Track",1);
        hashMetric.put("Av GPA overall",2);
        hashMetric.put("Av GPA overall Per Term",3);
        hashMetric.put("Av GPA overall Per course",4);
        hashMetric.put("Students, GPA",5);
        hashMetric.put("Course, Av GPA, #Students",6);

        List<String>listD=new ArrayList<String>(hashDegree.keySet());
        List<String>listM=new ArrayList<String>(hashMetric.keySet());


        ArrayAdapter adapterMetric = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, listM);
        spinnerMetric.setAdapter(adapterMetric);
        ArrayAdapter adapterDegree = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, listD);
        spinnerDegree.setAdapter(adapterDegree);

        final TextView txtresult=(TextView)findViewById(R.id.textViewRes);
        //txtresult.setText(spinnerDegree.getSelectedItem().toString());
        //txtresult.setText(editIP.getText().toString());

        spinnerDegree.setOnItemSelectedListener(new CustomOnItemSelectedListener());
        spinnerMetric.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                //d= hashDegree.get(spinnerDegree.getSelectedItem().toString()).toString();    //get value for selected keys
                d=spinnerDegree.getSelectedItem().toString(); //get ECE, CS
                m= hashMetric.get(spinnerMetric.getSelectedItem().toString()).toString();    //get value for selected Metric key
                //String r=executeRemoteCommand("hduser","yeah","192.168.1.101",3022);   //String username, String password, String hostname, int port
                try {
                   //sshAsyncTask sh=new sshAsyncTask("hduser","yeah","192.168.1.101",3022);
                    sshAsyncTask sh=new sshAsyncTask(editUser.getText().toString(),editPass.getText().toString(),
                            editIP.getText().toString(),Integer.parseInt( editPort.getText().toString()),m);
                           sh.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        btnMSG.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
               //Toast.makeText(getApplicationContext(),sshresultSring,Toast.LENGTH_LONG).show();

                //txtresult.setText(sshresultSring);
                //txtresult.setScrollY(txtresult.getLayout().getHeight());

                //String commandString=generateScript("1");
                //txtresult.setText(commandString);
                //to test selections from spinner
                //txtresult.setText(hashMetric.get(spinnerMetric.getSelectedItem().toString())
                //+" "+spinnerDegree.getSelectedItem().toString());

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {

            Spinner spin=(Spinner)findViewById(R.id.spinnerMetric);
            Toast.makeText(parent.getContext(),
                    //"Selected item : " + parent.getItemAtPosition(pos).toString(),
                    //Toast.LENGTH_SHORT).show();
                    "Selected item : " +hashMetric.get(spin.getSelectedItem().toString()).toString(),
                        Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }



    /*make ssh stuff an async task*/
    public class sshAsyncTask extends AsyncTask<Void, Void, String> {
        //String username, String password, String hostname, int port
        String username;
        String password;
        String hostname;
        int port;
        String metric;
        TextView txtres=(TextView)findViewById(R.id.textViewRes);

        public sshAsyncTask(String username, String password,String hostname,int port,String metric){
            this.username = username;
            this.password =password;
            this.hostname = hostname;
            this.port = port;
            this.metric=metric;
        }
        @Override
        protected void onPreExecute()
        {
            txtres.setText("Running Job . . .");
        }

        @Override
        protected String doInBackground(Void... params) {
            JSch jsch = new JSch();
            Session session = null;//22 default
            try {
                session = jsch.getSession(username, hostname, port);
            } catch (JSchException e) {
                e.printStackTrace();
            }
            session.setPassword(password);

            // Avoid asking for key confirmation
            Properties prop = new Properties();
            prop.put("StrictHostKeyChecking", "no");
            session.setConfig(prop);

            try {
                session.connect();
            } catch (JSchException e) {
                e.printStackTrace();
            }

            // SSH Channel
            ChannelExec channelssh = null;
            try {
                channelssh = (ChannelExec)
                        session.openChannel("exec");
            } catch (JSchException e) {
                e.printStackTrace();
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            channelssh.setOutputStream(baos);


            //CREATE BASH HADOOP COMMAND
            //String commandString=generateScript(metric);



            // Execute command
            //channelssh.setCommand("ls");
            //channelssh.setCommand(commandString);
            channelssh.setCommand("sh android_mapreduce.sh "+m+" "+d); //Hope for the best
            //channelssh.setCommand("sh android_mapreduce.sh 1 ECE"); //worked :)
            //channelssh.setCommand("cat dracula.txt | head");
            //channelssh.setCommand("echo \"hello from android\">>android.txt  ");
            try {
                channelssh.connect();
            } catch (JSchException e) {
                e.printStackTrace();
            }

            //INPUT STREAM
            InputStream inputStream = null;
            try {
                inputStream = channelssh.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder stringBuilder = new StringBuilder();

            String line;

            try {
                while ((line = bufferedReader.readLine()) != null)
                {

                    stringBuilder.append(line);
                    stringBuilder.append('\n');

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            // INPUT STREAM END..

            channelssh.disconnect();

            //return baos.toString();
            //return new String(baos.toByteArray());
            return stringBuilder.toString(); //INPUT STREAM
        }
        protected void onPostExecute(String result){
            sshresultSring=result;
            Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();
            txtres.setText(sshresultSring);
        }

    }

private String generateScript(String metric)
{
    String mapper="~/mapperm"+metric+".rb";
    String reducer="~/reducerm"+metric+".rb";
    /*String s="/usr/local/hadoop/bin/hadoop \\" +
            "jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.2.0.jar \\" +
            "-files"+mapper+","+reducer+" \\" +
            "-mapper "+mapper+" \\" +
            "-reducer "+reducer+" \\" +
            "-input /input/fakedata.json \\" +
            "-output /out";*/

    /*String s="/usr/local/hadoop/bin/hadoop "+
            "jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.2.0.jar " +
            "-files ~/mapperm1.rb,~/reducerm1.rb " +
            "-mapper ~/mapperm1.rb -reducer ~/reducerm1.rb " +
            "-input /input/fakedata.json -output /out"*/
    String s="sh android.sh "+ metric;


    //s="ls";
    return s;
}


/* ... */

       /* SSH STUFF
    public static String executeRemoteCommand(
            String username,
            String password,
            String hostname,
            int port) throws Exception {

        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, port);//22 default
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();

        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);

        // Execute command
        channelssh.setCommand("ls");
        //channelssh.setCommand("echo \"hello from android\">>android.txt  ");
        channelssh.connect();
        channelssh.disconnect();

        //return baos.toString();
        return new String(baos.toByteArray());
    }*/


}
